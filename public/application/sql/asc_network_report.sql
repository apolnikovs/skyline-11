/*Dropping temp tables and procedures, required for development in local client*/
DROP TEMPORARY TABLE IF EXISTS clients;
DROP TEMPORARY TABLE IF EXISTS result;
DROP TEMPORARY TABLE IF EXISTS statuses;
DROP TEMPORARY TABLE IF EXISTS statuses_result;
DROP PROCEDURE IF EXISTS getResult;
DROP PROCEDURE IF EXISTS getStatusResult;


/*SET @networkID = 4;*/
SET @networkID = [networkID];


/*Create temp table of Service Providers and Clients*/
CREATE TEMPORARY TABLE IF NOT EXISTS clients AS (
	SELECT		job.ServiceProviderID,
				sp.CompanyName,
				job.ClientID,
				client.ClientName
	FROM		job
	LEFT JOIN	service_provider AS sp ON job.ServiceProviderID = sp.ServiceProviderID
	LEFT JOIN	client ON job.ClientID = client.ClientID
	WHERE		job.ServiceProviderID IS NOT NULL AND
				job.ClientID IS NOT NULL AND
				job.NetworkID = @networkID
	GROUP BY	job.ServiceProviderID, job.ClientID
);


CREATE TEMPORARY TABLE IF NOT EXISTS result	(	
												serviceProviderID INT(11),
												serviceProviderName VARCHAR(100),
												clientID INT(11),
												clientName VARCHAR(100),
												lessThan7 INT(11),
												from7to14 INT(11),
												moreThan14 INT(11),
												moreThan30 INT(11),
												total INT(11)
											);

/*Create temp table of job Statuses of the Network*/
CREATE TEMPORARY TABLE IF NOT EXISTS statuses AS (
	SELECT		job.StatusID,
				status.StatusName
	FROM		job
	LEFT JOIN	status ON job.StatusID = status.StatusID
	WHERE		job.NetworkID = @networkID
	GROUP BY	job.StatusID
);

CREATE TEMPORARY TABLE IF NOT EXISTS statuses_result	(	
															id INT(11), 
															stat VARCHAR(100),
															lessThan7 INT(11),
															from7to14 INT(11),
															moreThan14 INT(11),
															moreThan30 INT(11),
															total INT(11)
														);
											

/*DELIMITER $$*/

/*Procedure to generate first report table - By Clients*/
CREATE PROCEDURE getResult()

BEGIN

  	DECLARE done INT DEFAULT FALSE;

	DECLARE serviceProviderID INT(11);
	DECLARE serviceProviderName VARCHAR(100);
	DECLARE clientID INT(11);
	DECLARE clientName VARCHAR(100);
	DECLARE lessThan7 INT(11);
	DECLARE from7to14 INT(11);
	DECLARE moreThan14 INT(11);
	DECLARE moreThan30 INT(11);
	DECLARE total INT(11);
	
	DECLARE cur CURSOR FOR SELECT * FROM clients;

 	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	OPEN cur;
	
	read_loop: LOOP
	
		FETCH cur INTO serviceProviderID, serviceProviderName, clientID, clientName;

		IF done THEN
      		LEAVE read_loop;
	   	END IF;	
		
		SET lessThan7 = (	
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) < 7 AND
									job.ClientID = clientID AND
									job.ServiceProviderID = serviceProviderID AND
									job.NetworkID = @networkID AND
									job.StatusID != 33 AND
									(job.ServiceProviderDespatchDate IS NULL OR job.ServiceProviderDespatchDate = "")
						);
						
		SET from7to14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) >= 7 AND
									DATEDIFF(CURDATE(), job.DateBooked) <= 14 AND
									job.ClientID = clientID AND
									job.ServiceProviderID = serviceProviderID AND
									job.NetworkID = @networkID AND
									job.StatusID != 33 AND
									(job.ServiceProviderDespatchDate IS NULL OR job.ServiceProviderDespatchDate = "")
						);
							 
		SET moreThan14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 14 AND
									job.ClientID = clientID AND
									job.ServiceProviderID = serviceProviderID AND
									job.NetworkID = @networkID AND
									job.StatusID != 33 AND
									(job.ServiceProviderDespatchDate IS NULL OR job.ServiceProviderDespatchDate = "")
						 );
		
		SET moreThan30 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 30 AND
									job.ClientID = clientID AND
									job.ServiceProviderID = serviceProviderID AND
									job.NetworkID = @networkID AND
									job.StatusID != 33 AND
									(job.ServiceProviderDespatchDate IS NULL OR job.ServiceProviderDespatchDate = "")
						 );
		
		SET total = lessThan7 + from7to14 + moreThan14;
						
		IF total > 0 THEN
			INSERT INTO result	(
									serviceProviderID,
									serviceProviderName,
									clientID,
									clientName,
									lessThan7, 
									from7to14, 
									moreThan14,
									moreThan30,
									total
								) 
			VALUES				(
									serviceProviderID,
									serviceProviderName,
									clientID,
									clientName,
									lessThan7, 
									from7to14, 
									moreThan14, 
									moreThan30, 
									total
								);
		END IF;
		
	END LOOP;

	CLOSE cur;
	
END;
/*END$$*/


/*Procedure to generate second report table - By Status*/
CREATE PROCEDURE getStatusResult()

BEGIN

  	DECLARE done INT DEFAULT FALSE;
  	
	DECLARE id INT(11);
	DECLARE stat VARCHAR(100);
	DECLARE lessThan7 INT(11);
	DECLARE from7to14 INT(11);
	DECLARE moreThan14 INT(11);
	DECLARE moreThan30 INT(11);
	DECLARE total INT(11);
	
	DECLARE cur CURSOR FOR SELECT * FROM statuses;
	
 	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
 	
	OPEN cur;
	
	read_loop: LOOP
	
		FETCH cur INTO id, stat;
		
		IF done THEN
      		LEAVE read_loop;
	   	END IF;	
		
		SET lessThan7 = (	
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) < 7 AND
									StatusID = id AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
						
		SET from7to14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) >= 7 AND
									DATEDIFF(CURDATE(), job.DateBooked) <= 14 AND
									StatusID = id AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
							 
		SET moreThan14 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 14 AND
									StatusID = id AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
		
		SET moreThan30 = (
							SELECT 	COUNT(*)
							FROM	job
							WHERE	DATEDIFF(CURDATE(), job.DateBooked) > 30 AND
									StatusID = id AND
									NetworkID = @networkID AND
									StatusID != 33 AND
									(ServiceProviderDespatchDate IS NULL OR ServiceProviderDespatchDate = "")
						);
		
		SET total = lessThan7 + from7to14 + moreThan14;
			
		IF total > 0 THEN
			INSERT INTO statuses_result	(id, stat, lessThan7, from7to14, moreThan14, moreThan30, total) 
			VALUES 						(id, stat, lessThan7, from7to14, moreThan14, moreThan30, total);
		END IF;
		
	END LOOP;
	
	CLOSE cur;

END;
/*END$$*/

/*DELIMITER ;*/


CALL getResult();
CALL getStatusResult();


SELECT * FROM result;
SELECT * FROM statuses_result;

DROP TEMPORARY TABLE IF EXISTS clients;
DROP TEMPORARY TABLE IF EXISTS result;
DROP TEMPORARY TABLE IF EXISTS statuses;
DROP TEMPORARY TABLE IF EXISTS statuses_result;
DROP PROCEDURE IF EXISTS getResult;
DROP PROCEDURE IF EXISTS getStatusResult;