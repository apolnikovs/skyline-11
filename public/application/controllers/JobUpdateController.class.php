<?php

require_once("CustomSmartyController.class.php");


class JobUpdateController extends CustomSmartyController {
    
    
    
    public $config;
    public $session;
    public $skyline;
    public $messages;
    public $lang = "en";
    public $page;
    public $statuses = [
	0 => ["Name" => "Active", "Code" => "Active"],
	1 => ["Name" => "In-active", "Code" => "In-active"]
    ];
    
    
    
    public function __construct() { 
        
        parent::__construct(); 
        
        $this->config = $this->readConfig("application.ini");
        
        $this->session = $this->loadModel("Session");
        
        $this->messages = $this->loadModel("Messages");
        
	if (isset($this->session->UserID)) {
	    
            $user_model = $this->loadModel("Users");
            $this->user = $user_model->GetUser($this->session->UserID);
	    $this->smarty->assign("loggedin_user", $this->user);
	    
            $this->smarty->assign("name", $this->user->Name);
            $this->smarty->assign("session_user_id", $this->session->UserID);
            $this->smarty->assign("_theme", $this->user->Skin);
            
            if ($this->user->BranchName) {
                $this->smarty->assign("logged_in_branch_name", " - " . $this->user->BranchName . " " . $this->config["General"]["Branch"] . " ");
            } else {
                $this->smarty->assign("logged_in_branch_name", "");
            }
            
            if ($this->session->LastLoggedIn) {
                $this->smarty->assign("last_logged_in", " - " . $this->config["General"]["LastLoggedin"] . " " . date("jS F Y G:i", $this->session->LastLoggedIn));
            } else {
                $this->smarty->assign("last_logged_in", "");
            } 
            
            $topLogoBrandID = $this->user->DefaultBrandID;
	    
        } else {
            
            $topLogoBrandID = isset($_COOKIE["brand"]) ? $_COOKIE["brand"] : 0;
            
            $this->smarty->assign("session_user_id", "");
            $this->smarty->assign("name", "");
            $this->smarty->assign("last_logged_in", "");
            $this->smarty->assign("logged_in_branch_name", ""); 
            $this->smarty->assign("_theme", "skyline");
            $this->redirect("index", null, null);
            
        } 
        
        if ($topLogoBrandID) {
            $skyline = $this->loadModel("Skyline");
            $topBrandLogo = $skyline->getBrand($topLogoBrandID);
            $this->smarty->assign("_brandLogo", (isset($topBrandLogo[0]["BrandLogo"])) ? $topBrandLogo[0]["BrandLogo"] : "");
        } else {
            $this->smarty->assign("_brandLogo", "");
        }
        $this->smarty->assign("showTopLogoBlankBox", false);
        
	if (isset($this->session->lang)) {
            $this->lang = $this->session->lang;
        }
	
    }
    
    
    
    public function getRAStatusesAction() {
	
	$jobModel = $this->loadModel("Job");
	$job = $jobModel->fetch($_POST["jobID"]);
	
	$raModel = $this->loadModel("RAStatusTypes");
	$result = $raModel->getRAStatusTypes($job["RAStatusID"], false, false, $_POST["type"]);
	
	$jobUpdateModel = $this->loadModel("JobUpdate");
	$status = $jobUpdateModel->getRAStatus($_POST["jobID"], $_POST["type"]);
	
	$return = new stdClass();
	$return->statuses = $result;
	$return->status = $status;
	
	echo(json_encode($return));
	
    }
    
    
    
    public function getAllRAStatusesAction() {
	
	$raModel = $this->loadModel("RAStatusTypes");
	$result = $raModel->getRAStatusTypes(false, false, $this->user->DefaultBrandID, $_POST["type"], false);
	echo(json_encode($result));
	
    }
    
    
}
?>