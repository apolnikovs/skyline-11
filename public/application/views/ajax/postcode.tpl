<label>{$page['Labels']['choose_address']|escape:'html'}:</label>
{if $addresses eq false}
No Address Found
{else}  
<select name="postSelect" class="postSelect">
    <option value="">{$page['Text']['please_select']|escape:'html'}</option>  
    {foreach $addresses as $row}
    <option title="" value="{$row[6]|lower|capitalize|escape:'html'} {$row[7]|lower|capitalize|escape:'html'}, {$row[5]|lower|capitalize|escape:'html'} {$row[0]|lower|capitalize|escape:'html'},{$row[2]|lower|capitalize|escape:'html'},{$row[3]|lower|capitalize|escape:'html'},{$row[12]|lower|capitalize|escape:'html'},{$row[13]|lower|capitalize|escape:'html'},{$row[4]|lower|capitalize|escape:'html'}" >
        {$row[11]|lower|capitalize|escape:'html'}
    </option>
    {/foreach}
</select>      
{/if}