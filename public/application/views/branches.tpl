{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Branches}
{/block}
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
        .ui-combobox-input {
             width:300px;
         }  
    </style>
{/block}

{block name=scripts}

    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    <script type="text/javascript" src="{$_subdomain}/js/functions.js"></script>
    
    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
                 
    var $clients = new Array();
       
        {foreach from=$allNetworkClients item=nClient key=networkId}
           
           $clients["{$networkId}"] = [        
            
            {foreach from=$nClient item=client} 
                   ["{$client.ClientID}", "{$client.ClientName|escape:'html'}"],
            {/foreach}
                
           ]
           
        {/foreach}      


    var $brands = new Array();
       
        {foreach from=$allClientBrands item=nBrand key=clientId}
           
           $brands["{$clientId}"] = [        
            
            {foreach from=$nBrand item=brand} 
                   ["{$brand.BrandID}", "{$brand.BrandName|escape:'html'}"],
            {/foreach}
                
           ]
           
        {/foreach} 

                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active and brands list for the branch.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[6]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(5)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(5)', nRow).html( $statuses[0][0] );
        }
        
        //Assigning brand names of particular branch.
        if(aData[5])
        {
            
           $.post("{$_subdomain}/OrganisationSetup/branches/getBrands/"+urlencode(aData[5]),        

            '',      
            function(data){

                   
                    var p = eval("(" + data + ")");

                    if(p[aData[5]])
                    {
                        $('td:eq(4)', nRow).html( p[aData[5]] );
                    }

            });
        }
        
    }
    
    
    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                if($("#nId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/branches/'+urlencode($("#nId").val())); 
                }
            }
        });
        $("#cId").combobox({
            change: function() {
                if($("#cId").val() != "") {
                    $(location).attr('href', '{$_subdomain}/OrganisationSetup/branches/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())); 
                }
            }
        });
        
               
                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/organisationSetup');

                                });


                       //Client compnay address lookup starts here..
                        $(document).on('click', '#quick_find_btn', 
                                        function() {

                            $.ajax( { type:'POST',
                                    dataType:'html',
                                    data:'postcode=' + $('#PostalCode').val(),
                                    success:function(data, textStatus) { 

                                            if(data=='EM1011' || data.indexOf("EM1011")!=-1)
                                            {

                                                    $('#selectOutput').html("<label class='fieldError' >{$page['Errors']['postcode_notfound']|escape:'html'}</label>"); 
                                                    $("#selectOutput").slideDown("slow");


                                                    $("#BuildingNameNumber").val('');	
                                                    $("#Street").val('');
                                                    $("#LocalArea").val('');
                                                    $("#TownCity").val('');

                                                    $("#PostalCode").focus();    
                                            }
                                            else
                                            {

                                                $('#selectOutput').html(data); 
                                                $("#selectOutput").slideDown("slow");
                                                $("#postSelect").focus();
                                            }




                                    },
                                    beforeSend:function(XMLHttpRequest){ 
                                        // $('#fetch').fadeIn('medium'); 
                                    },
                                    complete:function(XMLHttpRequest, textStatus) { 
                                        // $('#fetch').fadeOut('medium') 
                                            setTimeout("$('#quickButton').fadeIn('medium')");
                                    },
                                    url:'{$_subdomain}/index/addresslookup' 
                                } ); 
                            return false;


                        });                 
                        //Branch address lookup ends here..              







                     /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {

                                $(location).attr('href', '{$_subdomain}/OrganisationSetup/branches/'+urlencode($("#nId").val())); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
              

                      /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {

                                $(location).attr('href', '{$_subdomain}/OrganisationSetup/branches/'+urlencode($("#nId").val())+'/'+urlencode($("#cId").val())); 
                            }      
                        );*/
                    /* Add a change handler to the client dropdown - ends here*/
               
                       
                      
                   //change handler for network dropdown box.
                   /*$(document).on('change', '#NetworkID', 
                                function() {
                                    
                                  
                                    $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $NetworkID = $("#NetworkID").val();
                                
                                    if($NetworkID && $NetworkID!='')
                                        {
                                            $networkClients = $clients[$NetworkID];
                                           
                                            if($networkClients)
                                                {
                                                    for(var $i=0;$i<$networkClients.length;$i++)
                                                    {
                                                       
                                                        $clientDropDownList += '<option value="'+$networkClients[$i][0]+'" >'+$networkClients[$i][1]+'</option>';
                                                    }
                                                }
                                                
                                              
                                        }
                                        $("#ClientID").html($clientDropDownList); 
                                    

                                });*/        
                                
                        
                        //var  $brandCheckBoxesList = '';
                        //var  $brandRadioButtonsList = ''
                             
                                
                        //change handler for client dropdown box.
                        /*$(document).on('change', '#ClientID', 
                                function() {
                                  
                                  
                                   $brandRadioButtonsList = '';
                                   $brandCheckBoxesList = '';
                                
                                   var $ClientID = $("#ClientID").val();
                                    
                                     
                                
                                    if($ClientID && $ClientID!='')
                                        {
                                            $clientBrands = $brands[$ClientID];
                                           
                                            if($clientBrands)
                                                {
                                                    for(var $i=0;$i<$clientBrands.length;$i++)
                                                    {
                                                       //$brandCheckBoxesList += '<input  type="checkbox" name="BrandID[]"  value="'+$clientBrands[$i][0]+'" /><span class="text" >'+$clientBrands[$i][1]+'</span><br>';
                                                       $brandRadioButtonsList   += '<input  type="radio" name="BrandID"  value="'+$clientBrands[$i][0]+'" title="'+$clientBrands[$i][1]+'" /><span class="text" >'+$clientBrands[$i][1]+'</span><br>';
                                                    
                                                       $brandCheckBoxesList     += '<input class="bChkBox"  type="checkbox" name="BrandID[]"  value="'+$clientBrands[$i][0]+'" /><span class="text" >'+$clientBrands[$i][1]+'</span><br>';
                                                    }
                                                }
                                        }
                                        
                                      
                                       if($("#BranchType").val()=="Call Centre")
                                       {
                                            $("#BrandID").html($brandCheckBoxesList);
                                       }
                                       else
                                       {
                                            $("#BrandID").html($brandRadioButtonsList);  
                                       }
                                       

                                });*/
                                
                       
                       /* Add a click handler to the brand radio buttons - strats here*/
                        $(document).on('click', 'input[name=BrandID]', 
                            function() {
                            
                                
                                if($("#BranchLocation").length > 0)
                                {
                                    $("#BranchName").val(trim($(this).attr("title")+" "+$("#BranchLocation").val()));
                                }
                                else
                                {
                                    $("#BranchName").val($(this).attr("title"));
                                }
                                
                                getBrandSMS($(this).val());
                                
                            });
                       /* Add a click handler to the brand radio buttons - ends here*/      
                       
                       
                        $(document).on('click', '.bChkBox', 
                            function() {
                       
                             getBrandSMS('');
                             
                        });
                       
                       function getBrandSMS($brandID)
                       {
                            //getting brand sms.

                            $.post("{$_subdomain}/OrganisationSetup/branches/getBrandSMS/"+urlencode($brandID),        

                             '',      
                             function(data){

                                $smsDropDownList = '<option value="" selected="selected">Select from drop down</option>';
                                
                                var $smsList = eval("(" + data + ")");

                                if($smsList)
                                {

                                    for(var $i=0;$i<$smsList.length;$i++)
                                    {
                                           $smsDropDownList += '<option value="'+$smsList[$i]['SMSID']+'"   >'+$smsList[$i]['SMSName']+'</option>';
                                    }
                                }

                                $("#SMSID").html('');
                                $("#SMSID").html($smsDropDownList);

                                     

                             });
                            
                       }
                       
                       
                                

                       $(document).on('keyup', '#BranchLocation', 
                            function() {
                            
                                  if($("input[name='BrandID']:checked").val())
                                  {    
                                      $("#BranchName").val(trim($("input[name='BrandID']:checked").attr("title")+" "+$("#BranchLocation").val()));
                                  }
                                  else
                                  {
                                       $("#BranchName").val(trim($("#BranchLocation").val()));
                                  }
                           });


                        
                         /* Add a change handler to the county dropdown - strats here*/
                        /*$(document).on('change', '#BranchType', 
                            function() {
                            
                                $clientProperty =   $("#ClientID").attr("disabled");
                                    
                                if($clientProperty=="disabled") 
                                {    
                                    $("#ClientID").removeAttr("disabled");
                                }
                                
                                $("#ClientID").trigger("change");
                                
                                if($clientProperty=="disabled") 
                                {    
                                    $("#ClientID").attr("disabled", "disabled");
                                }
                                
                                if($("#BranchType").val()=="Call Centre")
                                {
                                     $("#BrandID").html($brandCheckBoxesList);
                                }
                                else
                                {
                                     $("#BrandID").html($brandRadioButtonsList);  
                                }
                            }      
                        );*/
                       /* Add a change handler to the county dropdown - ends here*/




                        /* Add a change handler to the county dropdown - strats here*/
                        /*$(document).on('change', '#CountyID', 
                            function() {


                                $("#CountyID option:selected").each(function () {
                                    $selected_cc_id =  $(this).attr('id');
                                    $country_id_array = $selected_cc_id.split('_');
                                    if($country_id_array[1]!='0')
                                    {
                                        $("#CountryID").val($country_id_array[1]);
                                        $("#CountryID").blur();
                                        $("#CountryID").attr("disabled","disabled").addClass('disabledField');
                                    }
                                    else
                                    {
                                        $("#CountryID").val('');
                                        $("#CountryID").removeAttr("disabled").removeClass('disabledField');
                                    }


                                });



                            }      
                        );*/
                       /* Add a change handler to the county dropdown - ends here*/


                                


                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                    

                    var  displayButtons = "UA";
                     
                     
                     

                     
                    
                    $('#BranchesResults').PCCSDataTable( {
                            aoColumns: [	
                                                        /* BranchID */  {  "bVisible":    false },    
                                                        /* CompanyName */   null,
                                                        /* ClientID */   null,
                                                        /* BranchNumber */   null,
                                                        /* BranchType */   null,
                                                        /* BrandID */   {  "bSortable": false, "bSearchable": false },
                                                        /* Status */  null
                                                ],
                            
                            aaSorting: [[ 1, "asc" ]],
                            displayButtons:  displayButtons,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/OrganisationSetup/branches/insert/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            createDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Branches/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                            
                            
                            frmErrorRules:   {
                                                    
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientID:
                                                        {
                                                            required: true
                                                        },
                                                    BranchName:
                                                        {
                                                            required: true
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: true
                                                        },
                                                   /* BuildingNameNumber:
                                                        {
                                                            required: true
                                                        },*/
                                                    Street:
                                                        {
                                                            required: true
                                                        },
                                                   
                                                    TownCity:
                                                        {
                                                            required: true
                                                        }, 
                                                    CountryID:
                                                        {
                                                            required: true
                                                        }, 
                                                    ContactEmail:
                                                        {
                                                            required: true,
                                                            email:true
                                                        }, 
                                                    ContactPhone:
                                                        {
                                                            required: true
                                                        }, 
                                                    BranchNumber:
                                                        {
                                                            required: true
                                                        }, 
                                                    AccountNo:
                                                        {
                                                            required: true
                                                        },    
                                                    BranchType:
                                                        {
                                                            required: true
                                                        }, 
                                                    BrandID:
                                                        {
                                                            required: true
                                                        },
						    ServiceAppraisalRequired: { required: true },
                                                    "BrandID[]":
                                                        {
                                                            required: true
                                                        },
                                                     OriginalRetailerFormElement:
                                                        {
                                                            required: true
                                                        },      
						    OpenJobsManagement: { required: true }
                                                        
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                    NetworkID:
                                                        {
                                                            required: "{$page['Errors']['service_network']|escape:'html'}"
                                                        },
                                                    ClientID:
                                                        {
                                                            required: "{$page['Errors']['client']|escape:'html'}"
                                                        },
                                                    BranchName:
                                                        {
                                                            required: "{$page['Errors']['name']|escape:'html'}"
                                                        },
                                                    PostalCode:
                                                        {
                                                            required: "{$page['Errors']['post_code']|escape:'html'}"
                                                        },
                                                   /* BuildingNameNumber:
                                                        {
                                                            required: "{$page['Errors']['building_name']|escape:'html'}"
                                                        },*/
                                                    Street:
                                                        {
                                                            required: "{$page['Errors']['street']|escape:'html'}"
                                                        },
                                                    TownCity:
                                                        {
                                                            required: "{$page['Errors']['town']|escape:'html'}"
                                                        }, 
                                                    CountryID:
                                                        {
                                                            required: "{$page['Errors']['country']|escape:'html'}"
                                                        },    
                                                        
                                                    ContactEmail:
                                                        {
                                                            required: "{$page['Errors']['email']|escape:'html'}",
                                                            email: "{$page['Errors']['valid_email']|escape:'html'}"
                                                        },  
                                                    ContactPhone:
                                                        {
                                                            required: "{$page['Errors']['phone']|escape:'html'}"
                                                        },     
                                                    BranchNumber:
                                                        {
                                                            required: "{$page['Errors']['branch_number']|escape:'html'}"
                                                        }, 
                                                    AccountNo:
                                                        {
                                                            required: "{$page['Errors']['account_number']|escape:'html'}"
                                                        },     
                                                    BranchType:
                                                        {
                                                            required: "{$page['Errors']['branch_type']|escape:'html'}"
                                                        },
                                                    BrandID:
                                                        {
                                                            required: "{$page['Errors']['radio_brand']|escape:'html'}"
                                                        },
						    ServiceAppraisalRequired: { required: "{$page['Errors']['service_appraisal_required']|escape:'html'}" },
                                                    "BrandID[]":
                                                        {
                                                            required: "{$page['Errors']['brand']|escape:'html'}"
                                                        },
                                                     OriginalRetailerFormElement:
                                                        {
                                                            required: "{$page['Errors']['original_retailer']|escape:'html'}"
                                                        },
						    OpenJobsManagement: { required: "This selection is mandatory." }        
                                                        
                                                        
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/OrganisationSetup/branches/update/',
                            updateDataUrl:   '{$_subdomain}/OrganisationSetup/ProcessData/Branches/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "BranchesForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'BranchesResultsPanel',
                            htmlTableId:     'BranchesResults',
                            fetchDataUrl:    '{$_subdomain}/OrganisationSetup/ProcessData/Branches/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                            dblclickCallbackMethod: 'gotoEditPage',
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            bottomButtonsDivId:'dataTables_command'


                        });
                      



                   

    });

</script>

<style>
    .ui-combobox-input {
	width: 300px;
    }
</style>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/organisationSetup" >{$page['Text']['organisation_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="BranchesTopForm" name="BranchesTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  
                 <br><br>
                        

                <div class="ServiceAdminResultsPanel" id="BranchesResultsPanel" >
                    
                    
                    <form id="nIdForm" class="nidCorrections">
                        
                    {if $User->NetworkID && $User->ClientID}
                
                       <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >
                       
                       <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >

                    {else if $User->NetworkID} 
                        
                         <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >
                          {$page['Labels']['client_label']|escape:'html'}
                          <select name="cId" id="cId" >
                                <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                                {foreach $nClients as $client}

                                    <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                         
                    {else if $SupderAdmin}
                            {$page['Labels']['service_network_label']|escape:'html'}
                            <select name="nId" id="nId" >
                                <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                                {foreach $networks as $network}

                                    <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                                {/foreach}
                            </select>
                                <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                            <select name="cId" id="cId" >
                                <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                                {foreach $nClients as $client}

                                    <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                                {/foreach}
                            </select>
                        
                    {/if}    

                  

                    </form>
                    
                    
                    <table id="BranchesResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                        <thead>
                                <tr>
                                        <th></th> 
                                        <th width="24%" title="{$page['Text']['name']|escape:'html'}" >{$page['Text']['name']|escape:'html'}</th>
                                        <th width="24%" title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                        <th width="80px" title="{$page['Text']['branch_number']|escape:'html'}" >{$page['Text']['branch_number']|escape:'html'}</th>
                                        <th width="90px" title="{$page['Text']['branch_type']|escape:'html'}" >{$page['Text']['branch_type']|escape:'html'}</th>
                                        <th title="{$page['Text']['brand']|escape:'html'}" >{$page['Text']['brand']|escape:'html'}</th>
                                        <th width="80px" title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>  
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        



    </div>
                        

{/block}



