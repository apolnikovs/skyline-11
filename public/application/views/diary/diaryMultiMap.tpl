{if !isset($appID)}
    {$appID=10}
    {/if}
   {$mapCount=$mapcount-1}<!-- total ammount of display maps default 5 (5+1)-->
   
   {if $mapCount==5}{$divider=2}{/if}
   {if $mapCount==3}{$divider=1}{/if}
   {if $mapCount==1}{$divider=1}{/if}
   
<html>
    <head>
        <style>
        #cboxLoadedContent    {
            padding:0 !important;
            
            }
            
        </style>
        <script>
            var mylatlong=0;
            var defLat=0;
            var defLong=0;
            var mapa1;
            var mapa2;
            var mapa3;
            var mapa4;
            var mapa5;
            var mapa6;
            var zoomlevel=11;
           
            $(document).ready(function() {
            
            setTimeout(function(){
    
   $("#cboxLoadedContent").css('width', $("#cboxLoadedContent").width());
        $(".multimapTd").css('background', 'none');
      
        
        
        //$('.multimap').css('height',$('.multimap').width())
    {if isset($adress)}
     var geocoder = new google.maps.Geocoder();
      geocoder.geocode({ 'address' : "{$adress}, UK"}, function(results, status){
  
     defLat = results[0].geometry.location.lat();
     defLong =results[0].geometry.location.lng() ;
     mylatlong=defLat+','+defLong;
    

{else}
    
 var defLat = 52.23793;
    var defLong = -0.90267 ;
    var mylatlong=defLat+','+defLong;
{/if}
    
    {if isset($appGeoTag)}
         defLat = {$appGeoTag.Latitude};
     defLong ={$appGeoTag.Longitude};
    mylatlong=defLat+','+defLong;
     
        {/if}
          
    var customMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/checkered_flag.png",new google.maps.Size(64, 64),  new google.maps.Point(0, 0),new google.maps.Point(6, 64));
    var TAMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/pointer3.png",new google.maps.Size(50, 60),  new google.maps.Point(0, 0),new google.maps.Point(25, 60));
    var amMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_b_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_g_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var anyMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_y_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var amTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_bt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var amSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_bs_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_gs_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var pmTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_gt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var atTMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_yt_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
    var atSMarker=new google.maps.MarkerImage("https://www.skylinecms.co.uk/images/diary/pointers_sv_ys_s.png",new google.maps.Size(25, 40),  new google.maps.Point(0, 0),new google.maps.Point(12, 40));
        
         
   
   
    {$c=1}
        {$dates=array()}
           
   {for $o=0 to $mapCount}
        var infowindow{$o+1} = new google.maps.InfoWindow();
   var    mapa{$o+1}= $('#map{$o+1}').gmap({ 'center':  mylatlong, 'zoom':12}).bind('init', function(event, map) { 
   
	$('#map{$o+1}').gmap('addMarker', { 'position': mylatlong, icon:customMarker,
    
'draggable': true , 
			'bounds': false,
                        id:'mapMarker{$o+1}',
                        title:"{if isset($adress)}{$adress}{/if}",
                        MAX_ZINDEX:1000
		}, function(map, marker) { 
               
            $.post("{$_subdomain}/Diary/setSessionGeoTag",{ 'lat':defLat,'lng':defLong});
                google.maps.event.addListener(marker, 'dblclick', function(event){
   
    map.setMapTypeId(google.maps.MapTypeId.HYBRID);
    map.setCenter(marker.getPosition()); // set map center to marker position
    smoothZoom(map, 19, map.getZoom()); // call smoothZoom, parameters map, final zoomLevel, and starting zoom level
    
})
  			//$('#dialog').append('');
			//findLocation(marker.getPosition(), marker,$('#map{$o+1}'));
		}).dragend( function(event) {
			findLocation(event.latLng, this,$('#map{$o+1}'));
		}).click( function() {
               
		
});//adding current app marker
        });
   $('#enter-full-screen{$o+1}').click(function(){
   {for $y=0 to $mapCount}
        $('#map{$y+1}').hide();
   {/for}
   $('#map{$o+1}').show();
   $('#map{$o+1}').css("position", 'fixed').
      css('top', 0).
      css('left', 0).
      css("width", $("#cboxLoadedContent").width()).
      css("height", $("#cboxLoadedContent").height());
    google.maps.event.trigger(mapa{$o+1}, 'resize');
    google.maps.event.trigger(mapa{$o+1}, 'resize');
    
    return false;
  });
 
  $('#exit-full-screen{$o+1}').click(function(){
    $('#map{$o+1}').css("position", 'relative').
      css('top', 0).
      css("width", googleMapWidth).
      css("height", googleMapHeight);
    google.maps.event.trigger(map, 'resize');
    map.setCenter(newyork);
    return false;
  });
        {/for}
            

            
             {foreach from=$waypoints key=k item=v}
         {$dates[]=$k}
        {if $c<$mapCount+2}
            {$skip=0}
   var LatLngList{$c} =new Array();
   
   {for $e=0 to $v|@count-1}
       {if isset($v[$e].latitude)}
          
   LatLngList{$c}[{$e}]=new google.maps.LatLng ({$v[$e].latitude},{$v[$e].longitude})
   //ading others markers
   
   {$marker=""}
        {if $v[$e].ta===1}icon:TAMarker,{/if}
                {if $v[$e].AppointmentTime=="ANY"}
                    
                {if $v[$e].manufacturer=="SAMSUNG" || $v[$e].manufacturer=="Samsung"}
                    {$marker=atSMarker}
                {elseif $v[$e].ta==1}
                    {$marker=atTMarker}
            {else}
                {$marker="anyMarker"}{/if}{/if}
                {if $v[$e].AppointmentTime=="AM"}
                    {if $v[$e].manufacturer=="SAMSUNG"||$v[$e].manufacturer=="Samsung"}
                    {$marker=amSMarker}
             {elseif $v[$e].ta===1}
                    {$marker=amTMarker}
            {else}    
            {$marker="amMarker"}{/if}{/if}
                {if $v[$e].AppointmentTime=="PM"}
                    {if $v[$e].manufacturer=="SAMSUNG"||$v[$e].manufacturer=="Samsung"}
                    {$marker=pmSMarker}
             {elseif $v[$e].ta===1}
                    {$marker=pmTMarker}
            {else}        
            {$marker="pmMarker"}{/if}{/if}
   
    $('#map{$c}').gmap('addMarker', { 'position': '{$v[$e].latitude},{$v[$e].longitude}',
   icon:{$marker},
    MAX_ZINDEX:100,
    popup:true
    }, function(map, marker) {
			google.maps.event.addListener(marker, 'click', function(){
       var content = '<br> <table style="background:none">';
       content+='<tr><td style="background:none"><span style="float:rigth;width:120px">Customer:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].name}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Time window:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].twindow}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Service Time:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].duration|date_format:"%H:%M"}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Manufacturer:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].manufacturer|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Engineer Name:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].engname|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Product Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].utName|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Time Slot:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].AppointmentTime|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Skill Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].reptype|escape:'html'}</span></td></tr>';
       content+='<tr><td style="background:none"><span style="float:rigth">Appointment Type:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].AppointmentType|escape:'html'}</span></td></tr>';
       content+='</table><hr><span style="float:rigth">Address:</span></td><td style="background:none"><span style="text-align:left">{$v[$e].adress|escape:'html'}</span>';
       infowindow{$c}.setContent(content);
                // z.log(map);
                 infowindow{$c}.maxWidth=350;
   infowindow{$c}.open(map,this);
    });                                           
                       
		 
});
       
        {elseif  $v|@count<=1}{$skip=1}{/if}
   {/for}
       
      // console.log(defLong);
    LatLngTarget=new google.maps.LatLng (defLat.toFixed(4),defLong.toFixed(4));
  
//  Create a new viewpoint bound
var bounds{$c} = new google.maps.LatLngBounds ();
//  Go through each...
{if $skip!=1}
for (var i = 0, LtLgLen = (LatLngList{$c}).length; i < LtLgLen; i++) {
  //  And increase the bounds to take this point
  if(LtLgLen>0){
  if((LatLngList{$c})[i]!=undefined){
   
  bounds{$c}.extend (LatLngList{$c}[i]);
  }
  }
}
{/if}
bounds{$c}.extend(LatLngTarget);

//zoom adjust
        var map{$c} = $("#map{$c}").gmap("get", "map");
        map{$c}.fitBounds( bounds{$c} );
        
         //zoom adjust
{$c=$c+1}
    {/if}
{/foreach}
    
    


  {if isset($adress)}});{/if}//geotager if inserting



}, 500);
        });
        
        
        
        function findLocation(location, marker,mapa) {
     
	mapa.gmap('search', { 'location': location}, function(results, status) {
		if ( status === 'OK' ) {
                 {for $o=0 to $mapCount}
                    
                   //  console.log(location);
               // $('#map{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].setPosition( new google.maps.LatLng( results[0].geometry.location.lat(), results[0].geometry.location.lng() ) );
                $('#map{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].setPosition(location) ;
                  {/for}
			
				$.post("{$_subdomain}/Diary/setSessionGeoTag",{ 'lat':location.lat(),'lng':location.lng()});
					
			
		}
	});
}

function insertApp(date,slot){
{if $insert==1}
$.post("{$_subdomain}/Diary/menuInsertAppointment",{ 'day':'1 '+date+' '+slot,'slot':slot},
 
function(data) {

$.colorbox({ html:data, title:"Add New Appointment",escKey: false,
    overlayClose: false,onComplete:function(data){
		
			$('#tfrom').timepicker();
		$('#tfrom2').timepicker();
    
    $('#tTo').timepicker();
    $('#tTo2').timepicker();
   }
      });
});
{else}
alert("This is view only mode, to insert appointment please use Service Base");
{/if}
}
function smoothZoom (map, max, cnt) {
    if (cnt >= max) {
            return;
        }
    else {
        z = google.maps.event.addListener(map, 'zoom_changed', function(event){
            google.maps.event.removeListener(z);
            self.smoothZoom(map, max, cnt + 1);
        });
        setTimeout(function(){ map.setZoom(cnt)}, 80); // 80ms is what I found to work well on my system -- it might not work well on all systems
    }
}
{$c=0}

var zoomlevel=10;
function zoomAllOut(){

 zoomlevel=zoomlevel-1;
{for $o=0 to $mapCount}
   // console.log($('#map{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition());
    $('#map{$o+1}').gmap('option', 'zoom', zoomlevel) ; 
    $('#map{$o+1}').gmap('option', 'center', $('#map{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition()) ; 
{/for}
}
function zoomAllIn(){
 zoomlevel=zoomlevel+1;
{for $o=0 to $mapCount}
    
   
     $('#map{$o+1}').gmap('option', 'zoom', zoomlevel) ; 
   
    $('#map{$o+1}').gmap('option', 'center', $('#map{$o+1}').gmap('get', 'markers')['mapMarker{$o+1}'].getPosition()) ; 
{/for}
}

function showAppointmentMap(date){
//It opens color box popup page.              
            $.colorbox( {   href: '{$_subdomain}/Diary/displayAppointmentMap/date='+date,
                            title: "Appointment Map",
                            opacity: 0.75,
                            width:$(window).width(),
                            height:$(window).height(),
                            overlayClose: false,
                            escKey: false
                            
            }); 
}

        </script>
        <style>
        .btnStandard{
            float:none;
            }
        </style>
    </head>
    
    <body>
        <div style="vertical-align: top;width:100%;height:100%;">
        <table >
            <tr>
                <td class="multimapTd"  style="background:#white;height:50px;width:50%"><image style="float:left;margin-top:0px" src="{$_subdomain}/css/Base/images/skyline-multimaps2.png">
               <div style="text-align:left;margin-top:50px;position:relative;margin-left: 50px">
                   <img  style="margin-left:50px;cursor:pointer;" src="{$_subdomain}/images/diary/zoom_out.png" onclick="zoomAllOut()" title="Zoom all maps out">
                    <img style="cursor:pointer;" src="{$_subdomain}/images/diary/zoom_in.png" onclick="zoomAllIn()" title="Zoom all maps in">
                   
                    </div>
                </td>
               
            <td style="background:#white;height:50px;width:300px">
            <image style="float:right;margin-right:100px;position:relative;margin-top:30px" src="{$_subdomain}/css/Base/images/viamente-multimaps2.png"></td>
            </td>
            
            </tr>
            </table>
            <table style="width:100%;height:80%;">
            <tr>
                {for $o=0 to $mapCount}
                <td class="multimapTd" style="width:33%;height:50%">
                 
                    <input {if isset($slotsType[$dates[$o]|date_format:"%d%m%Y"])}onclick="insertApp('{$dates[$o]|date_format:"%Y-%m-%d"}','{$slotsType[$dates[$o]|date_format:"%d%m%Y"]}');"{else}onclick="alert('Unfortunately you are unable to insert this appointment since you do not have any engineers available with the required skill on the selected day.'){/if};" type="checkbox" name="selectDay"> <span style="font-size:16px">{$dates[$o]|date_format:"%A %d %B"}</span>
                    <span style="font-size:16px;float:right">Slots left:    {if isset($slotsType[$dates[$o]|date_format:"%d%m%Y"])}{$slots[$dates[$o]|date_format:"%d%m%Y"]} {$slotsType[$dates[$o]|date_format:"%d%m%Y"]}{else}---{/if}</span>
                    <button class="btnStandard" onclick="showAppointmentMap('{$dates[$o]|date_format:"%Y-%m-%d"}')">Fullscreen</button>
                   
                    <div id="map{$o+1}" class="multimap" style="width:100%;height:90%;border: 1px solid black"></div>
                </td>
               {if $o==$divider&&$mapCount!=$divider}
            </tr>
             <tr>
               {/if}
                {/for}
            </tr>
        </table>
          </div>
            
    </body>
</html>