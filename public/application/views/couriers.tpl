{extends "DemoLayout.tpl"}


{block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $Couriers}
{/block}
    
    
    
{block name=afterJqueryUI}
    <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script> 
    <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
    <style type="text/css" >
    .ui-combobox-input {
        width:300px;
    }
    </style>
{/block}
    
    

{block name=scripts}
<style>
</style>

<script type="text/javascript">
    
    var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
    
    function inactiveRow(nRow, aData) {
    
        $('td:eq(3)', nRow).css('text-align','right').attr('title','{$page['Text']['day_hint']|escape:'html'}');
          
        if (aData[6]==$statuses[1][1]) {  
            $(nRow).addClass("inactive");
        } else {
            $(nRow).removeClass("inactive");
        }
    }
               
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }
    
    function ShowReconTable($Row) {
        $.colorbox({ href: '{$_subdomain}/LookupTables/recon/ID='+$Row[0], width: '720px' });
    }

    $(document).ready(function() {
        $("#spId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/LookupTables/couriers/'+urlencode($("#spId").val()));
            }
        });


                /* Add a change handler to the Service Provider dropdown - strats here*/
                    /*$(document).on('change', '#spId', 
                        function() {

                            $(location).attr('href', '{$_subdomain}/LookupTables/couriers/'+urlencode($("#spId").val())); 
                        }      
                    );*/
                  /* Add a change handler to the Service Provider dropdown - ends here*/


                

                  //Click handler for finish button.
                  $(document).on('click', '#finish_btn', 
                                function() {

                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/lookupTables');

                                });







                   /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        



                     

                   var  displayButtons = "UAP";
                     
                     
                     


                    
                    $('#CourierResults').PCCSDataTable( {

			aoColumns: [ 
			    /* CourierID */	    { mdata: 'CourierId',
                                                      bVisible: false }, 
                            /* Organisation Type */ { mdata: 'OrganisationType' }, 
			    /* Organisation Name */ { mdata: 'OrganisationName' },    
			    /* CourierName */	    { mdata: 'CourierName' },    
			    /* Day */	            { mdata: 'ReportFailureDays' },
                            /* Online */	    { mdata: 'Online' },
			    /* Status */	    { mdata: 'Status' }
			],
                            
                            aaSorting: [[ 1, "asc" ]],
                            displayButtons:  displayButtons,
                            pickButtonId:     'pickButtonId',
                            pickButtonText:   '{$page['Buttons']['pick']|escape:'html'}',
                            pickCallbackMethod: "ShowReconTable",
                            colorboxForceClose: false,
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/LookupTables/couriers/insert/',
                            createDataUrl:   '{$_subdomain}/LookupTables/ProcessData/Couriers/',
                            formInsertButton:'insert_save_btn',
                            
                            frmErrorRules:   {
                                            
                                                    OrganisationType:
                                                        {
                                                            required: true
                                                        },
                                                    CompanyID:
                                                        {
                                                            required: true
                                                        },
                                                    AccountNo:
                                                        {
                                                            required: true
                                                        },
                                                    ReportFailureDays:
                                                        {
                                                            digits: true
                                                        },    
                                                    Status:
                                                        {
                                                            required: true
                                                        },
						    CourierName: { required: true }
                                             },
                                                
                           frmErrorMessages: {
                                                
                                                   
                                                    OrganisationType:
                                                        {
                                                            required: "{$page['Errors']['organisation_type']|escape:'html'}"
                                                        },
                                                    CompanyID:
                                                        {
                                                            required: "{$page['Errors']['company']|escape:'html'}"
                                                        },
                                                    AccountNo:
                                                        {
                                                            required: "{$page['Errors']['account']|escape:'html'}"
                                                        },
                                                    ReportFailureDays:
                                                        {
                                                            digits: "{$page['Errors']['days_digits']|escape:'html'}"
                                                        },
                                                    Status:
                                                        {
                                                            required: "{$page['Errors']['status_error']|escape:'html'}"
                                                        },   
						    CourierName: { required: "{$page['Errors']['courier']|escape:'html'}" }
                                              },                     
                            
                            popUpFormWidth:  715,
                            popUpFormHeight: 330,
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/LookupTables/couriers/update/',
                            updateDataUrl:   '{$_subdomain}/LookupTables/ProcessData/Couriers/',
                            formUpdateButton:'update_save_btn',
                            colorboxFormId:  "CourierForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'CourierResultsPanel',
                            htmlTableId:     'CourierResults',
                            fetchDataUrl:    '{$_subdomain}/LookupTables/ProcessData/Couriers/fetch/'+urlencode("{$spId}"),
                            formCancelButton:'cancel_btn',
                           // pickCallbackMethod: 'openJob',
                            fnRowCallback:    'inactiveRow',
                            dblclickCallbackMethod: 'gotoEditPage',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            tooltipTitle:    "{$page['Text']['tooltip_title']|escape:'html'}",
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError"


                        });
                      



                   

    });

</script>

{/block}


{block name=body}

    <div class="breadcrumb">
        <div>
            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/lookupTables" >{$page['Text']['lookup_tables']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}
        </div>
    </div>



    <div class="main" id="home" >

               <div class="LTTopPanel" >
                    <form id="systemStatusesForm" name="systemStatusesForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  


                <div class="LTResultsPanel" id="CourierResultsPanel" >
                    
                    
                     <form name="spListsForm" id="spListsForm" >
                        {$page['Labels']['organisation_type_label']|escape:'html'}
                        <select name="spId" id="spId" >
                            <option value="" {if $spId eq ''}selected="selected"{/if}>{$page['Text']['select_organisation_type']|escape:'html'}</option>
                            {foreach $OrganisationTypes as $OrganisationType}
                            <option value="{$OrganisationType.Code}" {if $spId eq $OrganisationType.Code}selected="selected"{/if}>{$OrganisationType.Name|upper|escape:'html'}</option>
                            {/foreach}
                        </select>
                     </form>
                    

                    <table id="CourierResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
			<thead>
			    <tr>                               
                                <th title="{$page['Text']['courier_id']|escape:'html'}">{$page['Text']['courier_id']|escape:'html'}</th>
                                <th title="{$page['Text']['organisation_type']|escape:'html'}">{$page['Text']['organisation_type']|escape:'html'}</th>
                                <th title="{$page['Text']['organisation_name']|escape:'html'}">{$page['Text']['organisation_name']|escape:'html'}</th>
                                <th title="{$page['Text']['courier_name']|escape:'html'}">{$page['Text']['courier_name']|escape:'html'}</th>
                                <th title="{$page['Text']['day_hint']|escape:'html'}">{$page['Text']['day']|escape:'html'}</th>
                                <th title="{$page['Text']['online']|escape:'html'}">{$page['Text']['online']|escape:'html'}</th>
				<th title="{$page['Text']['status']|escape:'html'}">{$page['Text']['status']|escape:'html'}</th>
			    </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>  
                                
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                </div>        


    </div>
                        
                        



{/block}



