<?php
/**
 * ServiceProviderEngineer.class.php
 * 
 * Routines for interaction with the ServiceProviderEngineer table
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 PC Control Systems
 * @link       
 * @version    1.00
 * 
 * Changes
 * Date        Version Author                Reason
 * 19/10/2012  1.00    Andrew J. Williams    Initial Version
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class ServiceProviderEngineer extends CustomModel { 
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Database Connection */
    public $debug = false;
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        
        $this->table = TableFactory::ServiceProviderEngineer();
    }
    
    
    /**
     * getIdByCode
     *  
     * Return ID of a specific engineer for a specific provider given the 
     * engineers code
     * 
     * @param $eCode    The Engineer Code (eg Servicebase user code)
     *        $spId     The Service Provider Engineer
     * 
     * @return      Array containing recrd selected 
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function getIdByCode($eCode, $spId) {
        $sql = "
                SELECT 
                        `ServiceProviderEngineerID`
                FROM 
                        `service_provider_engineer`
                WHERE 
                        `ServiceBaseUserCode` = '$eCode'
                        AND `ServiceProviderID` = $spId
               ";
        
        $result = $this->Query($this->conn, $sql);

        if ( count($result) > 0 ) {
            return($result[0]['ServiceProviderEngineerID']);                                                 /* Entry founs so return record*/
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
    
}

?>
