<?php
require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * StatusHistory.class.php
 * 
 * This model handles interacttion with the status_history table.
 * 
 * @author      Andrew J. Williams <a.williams@pccsuk.com>
 * @version     1.10
 * @copyright   2012 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author                Reason
 * 22/06/2012  1.00    Andrew J. Williams    Initial Version
 * 02/08/2012  1.01    Nageswara Rao Kanteti Added getStatusHistory
 * 15/08/2012  1.02    Andrew J. Williams    Added getLastStatus
 * 24/10/2012  1.03    Andrew J. Williams    Added getStatusChangeReport
 * 26/10/2012  1.04    Brian Etherington     Added getJobStatusHistory
 * 29/10/2012  1.05    Andrew J. Williams    Issue 113 - Duplicate child files in Appointment and Status History
 * 09/11/2012  1.06    Andrew J. Williams    Changes to getStatusChangeReport
 * 19/11/2012  1.07    Andrew J. Williams    Removed getStatusChangeReport now done programatically in DataController
 * 19/04/2013  1.08    Andrew J. Williams    Data Integrity Check - Refresh Process
 * 01/05/2013  1.09    Andrew J. Williams    Issue 341 - Add count of each status in job to the Status Change Report
 * 03/05/2013  1.10    Andrew J. Williams    Issue 349 - Urgent changes for Skyline to RMA
 ******************************************************************************/

class StatusHistory extends CustomModel {
    private $table;                                                             /* For Table Factory Class */
    private $conn;
    #public $debug = true;
    
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

        $this->table = TableFactory::StatusHistory();
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used to get status history for given job and status.
     *
     * @param int $JobID
     * @param int $StatusID 
     * @return array() 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function getStatusHistory($JobID, $StatusID, $OrderBy="ORDER BY StatusHistoryID") {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT StatusHistoryID, UserID, Date, AdditionalInformation FROM status_history WHERE JobID=:JobID AND StatusID=:StatusID '.$OrderBy.' LIMIT 0,1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        $fetchQuery->execute(array(':StatusID' => $StatusID, ':JobID' => $JobID));
        $result = $fetchQuery->fetch();
        
        return $result;
     }

    /**
     * create
     *  
     * Create a part
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new Status History Item
     * 
     * @return (status - Status Code, message - Status message, id - ID of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'SUCCESS',
                             'statusHistoryId' => $this->conn->lastInsertId());
        } else {
            $result = array('status' => 'FAIL',
                            'statusHistoryId' => 0,
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
    
    /**
     * upadate
     *  
     * Update status history
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new Status History Item
     * 
     * @return (status - Status Code, message - Status message, id - ID of inserted item)
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->debug) {
            $this->controller->log( $cmd );
            $this->controller->log( $args );
        }
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result =  array('status' => 'SUCCESS',
                             'statusHistoryId' => $this->conn->lastInsertId());
        } else {
            $result = array('status' => 'FAIL',
                            'statusHistoryId' => 0,
                            'message' => $this->lastPDOError());          
        }
        return $result;
    }
   
    /**
     * deleteByJobId
     *  
     * Delete all the status history for a specific job.
     * 
     * This is specifcally used in Data Integrity Check Refresh Process
     * 
     * Note Issue 349 - https://bitbucket.org/pccs/skyline/issue/349/urgent-changes-for-skyline-to-rma
     * Does not remove '02 ALLOCATED TO AGENT' or '03 TRANSMITTED TO AGENT' as these are done on Skyline and will not be on Servicebase
     * (See Chris Berry for Details)
     * '02 ALLOCATED TO AGENT' = 2
     * '03 TRANSMITTED TO AGENT' = 3
     * 
     * @param array $jId    JobID
     * 
     * @return true if sucessful, false otherwise
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function deleteByJobId($jId) {
        $sql = "
                DELETE
		FROM
			`status_history`
		WHERE
			`JobID` = $jId
                         AND `StatusID` != 2
                         AND `StatusID` != 3
               ";
        
        if ($this->debug) $this->controller->log($sql);
        
        $count = $this->conn->exec($sql);

        if ( $count !== false ) {
            return(true);                         
        } else {
            return(false);                                 
        }
    }
    
    /**
     * getLastStatus
     *  
     * Get the last status of a job
     * 
     * @param array $jId    Job ID
     * 
     * @return string   Status name
     * 
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    public function getLastStatus($jId, $all=false) {
        $sql = "
                SELECT
                	s.`StatusName`,
                	MAX(sh.`Date`) as StatusDate
                FROM 
                	`status_history` sh,
                        `status` s
                WHERE
                        sh.`StatusID` = s.`StatusID`
                	AND sh.`JobID` = $jId 
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ($all) {
            return $result;
        } elseif ( count($result) > 0 ) {
            return($result[0]['StatusName']);                                   /* Status exists so return ststus name */
        } else {
            return(null);                                                       /* Not found return null */
        }
    }
       
    
    /**
     * getJobStatusHistory
     *  
     * Get the status history of a job
     * 
     * @param array $job_id    Job ID
     * 
     * @return string   Status name
     * 
     * @author Brian Etherington <b.etherington@pccsuk.com>  
     **************************************************************************/
    public function getJobStatusHistory($job_id) {
        
        $sql = "select sh.Date as StatusDate, s.StatusName from status s left join status_history sh on s.StatusID=sh.StatusID where sh.JobID=:JobID order by sh.Date desc";
        $params = ['JobID' => $job_id];
        return $this->query($this->conn, $sql, $params);
	
    }
    
    
    /**
     * doesStatusUpdatetExist
     *  
     * Returns whether an status update with a given Job ID exists.
     * 
     * @param $jId  Job ID
     *        $cdt  Date and Time
     *        $sId  Status ID
     * 
     * @return      Boolean containing whether the status item exists
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function doesStatusUpdatetExist($jId, $cdt, $sId) {
        $sql = "
                SELECT
			COUNT(`StatusHistoryID`) AS `count`
		FROM
		        `status_history`
		WHERE
			`JobID` = $jId
			AND `Date` = '$cdt'
			AND `StatusID` = $sId
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        if ($result[0]['count'] == 0) {
            return(false);
        } else {
            return (true);
        }
    
    }
    
    
    /**
     * countStatusEntryForJob
     *  
     * Returns the number of status update with a given Job ID exists.
     * 
     * @param $jId      Job ID
     *        $sName  Status Name
     * 
     * @return      The number of times this job has been at this status
     *
     * @author Andrew Williams <a.williams@pccsuk.com>  
     **************************************************************************/
    
    public function countStatusEntryForJob($jId, $sName) {
        $sql = "
                SELECT
			COUNT(`StatusHistoryID`) AS `count`
		FROM
		        `status_history` sh LEFT JOIN `status` s ON sh.`StatusID` =  s.`StatusID`
		WHERE
			sh.`JobID` = $jId
			AND `StatusName` = '$sName'
               ";
        
        $result = $this->Query($this->conn, $sql);
        
        return($result[0]['count']);
    }
    
}
?>