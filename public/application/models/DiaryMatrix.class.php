<?php

/**
 * DiaryMatrix.class.php
 * 
 * This model handles interacttion with the diary_matrix table.
 * 
 * @author      Andrew J. Williams <Andrew.Williams@awcomputech.com>
 * @version     1.0
 * @copyright   2013 PC Control Systems Ltd
 * 
 * Changes
 * Date        Version Author               Reason
 * 26/04/2013  1.00    Andrew J. Williams   Initial Version
 ******************************************************************************/

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

class DiaryMatrix extends CustomModel { 
    
    private $table;                                                             /* For Table Factory Class */
    private $conn;                                                              /* Datbase connection */
    
    public function __construct($Controller) {
                  
        parent::__construct($Controller);
        
        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );  
        
        $this->table = TableFactory::DiaryMatrix();
    }
       
    /**
     * create
     *  
     * Create a Diary Matrix item
     * 
     * @param array $args   Associative array of field values for the creation of
     *                      the new diary matrix
     * 
     * @return array    (status - Status Code, message - Status message, id - Id of inserted item
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com> 
     **************************************************************************/
    public function create($args) {
        $cmd = $this->table->insertCommand( $args );
        if ( $this->Execute($this->conn, $cmd, $args) ) {            
            $result = array (
                             'status' => 'SUCCESS',
                             'id' => $this->conn->lastInsertId()
                            );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'id' => 0,
                            'message' => $this->lastPDOError()
                           );
        }
        
        return ( $result );
    }
    
    /**
     * update
     *  
     * Update a ContactHistoryItem
     * 
     * @param array $args   Associative array of field values for to update the
     *                      appointment. The array must include the primary key
     *                      DiaryMatrixID
     * 
     * @return (status - Status Code, message - Status message)
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com> 
     **************************************************************************/
    public function update($args) {
        $cmd = $this->table->updateCommand( $args );
        
        if ($this->Execute($this->conn, $cmd, $args)) {
            $result = array(
                            'status' => 'SUCCESS',
                            'message' => ''
                           );
        } else {
            $result = array(
                            'status' => 'FAIL',
                            'message' => $this->lastPDOError()
                           );          
        }
        return $result;
    }
    
    /**
     * delete
     *  
     * Delete a Call History item. As Service base does not know ContactHistoryID
     * we need to delete on date, time and JobID as this is unique.
     * 
     * @param Date $dmId    Diary Matrix ID
     * 
     * @return (status - Status Code, message - Status message, rows_affected number of rows deleted)
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com> 
     **************************************************************************/
    public function delete($dmId) {      
        $cmd = $this->table->deleteCommand("
                                            `DiaryMatrixID` = $dmId
                                           "
                                          );
        
        $rows_affected = $this->Execute($this->conn, $cmd);
        
        if ( $rows_affected == 0 ) {                                             /* No rows affected may be error */
            $result = array(
                            'status' => 'FAIL',
                            'message' => 'No rows deleted'
                           ); 
        } else {
            $result = array(
                            'status' => 'SUCCESS',
                            'message' => 'Deleted'
                           ); 
        }
        
        return ( $result );
    }
    
    
    /**
     * GetName
     *  
     * Get the Matrix name for a given service provider, date and key.
     * We must include the key here now as Andris has changed thdieay system to
     * allow the use of two keys
     * 
     * @param integer $spId The ID of Service Proder we are interested in
     * @param Date $date    The date for which we are cecking
     * @param string $key   The relevant Viamente Key
     * 
     * @return nAme of matrix or null if not found  
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com> 
     **************************************************************************/
    public function GetName($spId, $date, $key) {      
        $sql = "
                SELECT
			`Name`
		FROM
		        `diary_matrix`
		WHERE
                        `ServiceProviderID` = $spId
                        AND `Date` = '$date'
                        AND `Key` = '$key'
               ";

        $result = $this->Query($this->conn, $sql);
        
        if (count($result) > 0 ) {                                              /* Check if we have results */
            return($result[0]['Name']);                                         /* Yes - return matrix name */
        } else {
            return(null);                                                       /* No - return null*/
        }
        
        return($result[0]['count']);
    }
    
    
    /**
     * GetName
     *  
     * Get the Matrix name for a given service provider, date and key.
     * We must include the key here now as Andris has changed thdieay system to
     * allow the use of two keys
     * 
     * @param Date $date    The date for which we are checking
     * 
     * @return 
     * 
     * @author Andrew Williams <Andrew.Williams@awcomputech.com> 
     **************************************************************************/
    public function DeleteAllOnAndBefore($spId, $date) {  
        $api_viemente_model = $this->controller->LoadModel('APIViamente');
        
        $sql = "
                SELECT
			`Name`
		FROM
		        `diary_matrix`
		WHERE
                        `ServiceProviderID` = $spId
                        AND `Date` <= '$date'
               ";

        $result = $this->Query($this->conn, $sql);
        
        if (count($result) > 0 ) {                                              /* Check if we have results */
            foreach ($result as $row) {                                         /* Loop through each record */
                $api_viemente_model->viamenteCall(                              /* Remove the matrix from Viamente */
                                                    '', 
                                                    "/opt/v3/matrix/name/{$row['Name']}", 
                                                    $row['Key'], 
                                                    'DELETE'
                                                  );
                                                            
                $this->delete($row['DiaryMatrixID']);                           /* Delte the record from the DiaryMatroix table */
            }
        }
    }
    
    
    
}
?>
