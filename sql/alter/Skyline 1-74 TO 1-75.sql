# ---------------------------------------------------------------------- #
# Script generated with: DeZign for Databases V7.2.0                     #
# Target DBMS:           MySQL 5                                         #
# Project file:          SkyLine.dez                                     #
# Project name:          SkyLine                                         #
# Author:                Brian Etherington                               #
# Script type:           Alter database script                           #
# Created on:            2012-08-24 15:34                                #
# ---------------------------------------------------------------------- #


# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                                     #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.74');

# ---------------------------------------------------------------------- #
# Drop foreign key constraints                                           #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` DROP FOREIGN KEY `county_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `country_TO_customer`;

ALTER TABLE `customer` DROP FOREIGN KEY `customer_title_TO_customer`;

ALTER TABLE `samsung_claim_response` DROP FOREIGN KEY `job_TO_samsung_claim_response`;

ALTER TABLE `job` DROP FOREIGN KEY `customer_TO_job`;

ALTER TABLE `town_allocation` DROP FOREIGN KEY `customer_TO_town_allocation`;

# ---------------------------------------------------------------------- #
# Drop table "samsung_claim_response"                                    #
# ---------------------------------------------------------------------- #

# Remove autoinc for PK drop #

ALTER TABLE `samsung_claim_response` MODIFY `SamsungClaimResponseID` INTEGER NOT NULL;

# Drop constraints #

ALTER TABLE `samsung_claim_response` DROP PRIMARY KEY;

# Drop table #

DROP TABLE `samsung_claim_response`;

# ---------------------------------------------------------------------- #
# Modify table "customer"                                                #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` MODIFY `SecurityQuestionID` INTEGER;

ALTER TABLE `customer` MODIFY `SecurityQuestionAnswer` VARCHAR(256);

# ---------------------------------------------------------------------- #
# Add table "claim_response"                                             #
# ---------------------------------------------------------------------- #

CREATE TABLE `claim_response` (
    `ClaimResponseID` INTEGER NOT NULL AUTO_INCREMENT,
    `JobID` INTEGER NOT NULL,
    `ErrorCode` VARCHAR(18),
    `ErrorDescription` VARCHAR(50),
    CONSTRAINT `claim_responseID` PRIMARY KEY (`ClaimResponseID`)
)
ENGINE = INNODB
DEFAULT CHARACTER SET = latin1
COLLATE = latin1_swedish_ci;

CREATE INDEX `IDX_claim_response_JobID_FK` ON `claim_response` (`JobID`);

# ---------------------------------------------------------------------- #
# Add foreign key constraints                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `customer` ADD CONSTRAINT `county_TO_customer` 
    FOREIGN KEY (`CountyID`) REFERENCES `county` (`CountyID`);

ALTER TABLE `customer` ADD CONSTRAINT `country_TO_customer` 
    FOREIGN KEY (`CountryID`) REFERENCES `country` (`CountryID`);

ALTER TABLE `customer` ADD CONSTRAINT `customer_title_TO_customer` 
    FOREIGN KEY (`CustomerTitleID`) REFERENCES `customer_title` (`CustomerTitleID`);

ALTER TABLE `claim_response` ADD CONSTRAINT `job_TO_claim_response` 
    FOREIGN KEY (`JobID`) REFERENCES `job` (`JobID`);

ALTER TABLE `job` ADD CONSTRAINT `customer_TO_job` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

ALTER TABLE `town_allocation` ADD CONSTRAINT `customer_TO_town_allocation` 
    FOREIGN KEY (`CustomerID`) REFERENCES `customer` (`CustomerID`);

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.75');
