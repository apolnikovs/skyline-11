# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.283');

# ---------------------------------------------------------------------- #
# Andris Stock Changes   												 #
# ---------------------------------------------------------------------- # 
ALTER TABLE service_provider ADD COLUMN ChangeSupplieronJobPartOrder ENUM('Yes','No') NOT NULL DEFAULT 'No' AFTER DocLogo;

ALTER TABLE part ADD COLUMN ChargeType ENUM('W','C','E') NULL AFTER SPPartOrderID;



# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.284');
