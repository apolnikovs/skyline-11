# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.301');

# ---------------------------------------------------------------------- #
# Praveen Kumar Nakka Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE unit_type ADD IMEILengthFrom INT( 11 ) NOT NULL AFTER ImeiRequired , ADD IMEILengthTo INT( 11 ) NOT NULL AFTER IMEILengthFrom;


# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE user_reports ADD COLUMN UserTypeID INT(11) NULL DEFAULT NULL AFTER UserType;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.302');



