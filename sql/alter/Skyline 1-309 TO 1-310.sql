# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.309');

# ---------------------------------------------------------------------- #
# Krishnam Raju Nalla Changes 											 #
# ---------------------------------------------------------------------- # 
INSERT INTO national_bank_holiday (HolidayDate, HolidayName, CountryID) VALUES ('2013-01-01', 'New Year\'s Day', '2'), ('2013-03-18', 'St Patricks Day (in lieu)', '2'), ('2013-03-29', 'Good Friday', '2'), ('2013-04-01', 'Easter Monday', '2'), ('2013-05-06', 'May Day', '2'), ('2013-06-03', 'June Bank Holiday', '2'), ('2013-08-05', 'August Bank Holiday', '2'), ('2013-10-28', 'October Bank Holiday', '2'), ('2013-12-25', 'Christmas Day', '2'), ('2013-12-26', 'St Stephens Day', '2'), ('2013-05-06', 'Early May Bank Holiday', '3'), ('2013-05-27', 'Spring bank holiday', '3'), ('2013-08-05', 'Summer bank holiday', '3'), ('2013-12-02', 'St Andrew\'s Day (substitute day)', '3'), ('2013-12-25', 'Christmas Day', '3'), ('2013-12-26', 'Boxing Day', '3'), ('2014-01-01', 'New Year\'s Day', '3'), ('2014-01-02', '2nd January', '3'), ('2014-04-18', 'Good Friday', '3');


# ---------------------------------------------------------------------- #
# Vykintas Rutkunas Changes 											 #
# ---------------------------------------------------------------------- # 
INSERT INTO help_text (HelpTextCode, HelpTextTitle, HelpText) VALUES ('ReportPrimaryTableInfo', 'Primary Table Info', 'Primary Table Info'); INSERT INTO help_text (HelpTextCode, HelpTextTitle, HelpText) VALUES ('ReportSecondaryTableInfo', 'Secondary Table Info', 'Secondary Table Info'); INSERT INTO help_text (HelpTextCode, HelpTextTitle, HelpText) VALUES ('ReportMultipleTableInfo', 'Multiple Table Info', 'Multiple Table Info');

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.310');



#test